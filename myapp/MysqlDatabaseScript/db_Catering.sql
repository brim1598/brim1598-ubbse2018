-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 10, 2019 at 12:23 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_Catering`
--

-- --------------------------------------------------------

--
-- Table structure for table `dt_Kategoriak`
--

CREATE TABLE `dt_Kategoriak` (
  `_kategoriaid` int(11) NOT NULL,
  `_kategorianev` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dt_Kategoriak`
--

INSERT INTO `dt_Kategoriak` (`_kategoriaid`, `_kategorianev`) VALUES
(1, 'Levesek'),
(2, 'Koretek'),
(3, 'Sultek'),
(4, 'Fozelek'),
(5, 'Hidegtalak');

-- --------------------------------------------------------

--
-- Table structure for table `dt_Kepek`
--

CREATE TABLE `dt_Kepek` (
  `_kepid` int(11) NOT NULL,
  `_path` varchar(500) DEFAULT NULL,
  `_menuid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dt_Kepek`
--

INSERT INTO `dt_Kepek` (`_kepid`, `_path`, `_menuid`) VALUES
(1, 'images/anaszoskaposzta.JPG', 2),
(2, 'images/babfozelek.jpg', 1),
(3, 'images/bundaszsemle.jpg', 3),
(4, 'images/fofogastorta.jpg', 4),
(5, 'images/gulyasleves.jpg', 6),
(6, 'images/karajbazartcsulok.jpg', 5);

-- --------------------------------------------------------

--
-- Table structure for table `dt_Megjegyzesek`
--

CREATE TABLE `dt_Megjegyzesek` (
  `_megjegyzesid` int(11) NOT NULL,
  `_megjegyzes` varchar(1000) DEFAULT NULL,
  `_menuid` int(11) DEFAULT NULL,
  `_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dt_Megjegyzesek`
--

INSERT INTO `dt_Megjegyzesek` (`_megjegyzesid`, `_megjegyzes`, `_menuid`, `_date`) VALUES
(1, '10/10', 1, '2018-12-03'),
(2, 'Megpróbáljuk arányosítani a kihívást jelentõ piaci trendeket.', 1, '2018-12-01'),
(3, 'Elemzések segítségével kell életre kelteni a dinamikusan növekvõ szegmentumokat.', 3, '2018-12-18'),
(4, 'Formállogikai alapon kellene kihasználni a XXI. századi színvonalú kontrollingot.', 2, '2018-12-16'),
(5, 'Alkotó folyamat keretében kell szintetizálni a vállalható csapatot.', 5, '2018-12-15'),
(6, 'stabil ez a kaja dilo!', 4, '2018-12-21'),
(7, 'ragos volt a paradicsom', 6, '2018-12-21'),
(8, 'Formállogikai alapon kell dinamikusan növekvõ szegmentumokat.\r\n', 3, '2018-12-21'),
(9, 'Formállogikai alapon kell dinamikusan növekvõ szegmentumokat.\r\n', 3, '2018-12-21'),
(10, 'megvettem de nem szeretem', 1, '2018-12-21'),
(11, 'a maradekot nem adtak ide', 2, '2018-12-21'),
(12, 'finom', 4, '2018-12-21');

-- --------------------------------------------------------

--
-- Table structure for table `dt_Menuk`
--

CREATE TABLE `dt_Menuk` (
  `_menuid` int(11) NOT NULL,
  `_menunev` varchar(100) NOT NULL,
  `_leiras` varchar(500) NOT NULL,
  `_kategoriaid` int(11) DEFAULT NULL,
  `_ar` varchar(10) NOT NULL,
  `_elkeszitesiido` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dt_Menuk`
--

INSERT INTO `dt_Menuk` (`_menuid`, `_menunev`, `_leiras`, `_kategoriaid`, `_ar`, `_elkeszitesiido`) VALUES
(1, 'Babfozelek', '50 dkg bab 1 fej vöröshagyma 2 gerezd fokhagyma 1 kiskanál mustár babérlevél, pirospaprika 2 evőkanál liszt zsiradék 1 dl tejföl ecet, pici cukor', 4, '20', '01:00:00'),
(2, 'Ananászos savanyú káposzta', '80 dkg savanyú káposzta\r\n1 ananász konzerv (vagy friss ananász)\r\n1 fej vöröshagyma\r\n2 db babérlevél\r\n4-5 db borókabogyó\r\nőrölt bors\r\n1 ek kristálycukor\r\n2 dl fehérbor\r\n1,5-2 dl ananászlé (vagy víz)\r\n2 ek olaj', 2, '20', '00:30:00'),
(3, 'Bundás zsemle', '4 zsemle\r\n3-4 tojás\r\nsó, bors ízlés szerint\r\nkb 0,5 dl tej\r\nolaj a sütéshez\r\ntejföl és reszelt sajt\r\n', 2, '15', '02:00:00'),
(4, 'Főfogás torta', '1 liter friss, forró rizibizi\r\n1,5 kg burgonyából friss, forró krumplipüré\r\n6 sültkolbász\r\n10-12 rántott szelet\r\nuborka szeletek\r\nparadicsom cikkek\r\ntubusos (csípős) pirosarany\r\nbazsalikom levelek', 5, '40', '00:45:00'),
(5, 'Karajba zárt csülök', '1 füstölt csülök\r\n9–10 szelet karaj\r\nkb 20 szelet bacon\r\n1 nagy fej vöröshagyma\r\n2 evőkanál mustár\r\nkb 20 dkg reszelt sajt\r\n2 tojás\r\nliszt', 5, '17', '01:30:00'),
(6, 'Gulyás leves', '1 kg marhahús\r\n2 db sertés oldalas\r\n100 g szalonna vagy egy jó evőkanál zsír\r\n250 g vöröshagyma\r\n3 szál sárgarépa\r\n2 szál petrezselyemgyökér\r\n1 nagy tv paprika\r\n3 paradicsom\r\n1 nagy evőkanál pirospaprika\r\n2 babérlevél\r\nsó, bors, friss erőspaprika darálva\r\n700 g krumpli\r\nlehet még tenni köménymagot, fokhagymát', 1, '15', '01:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `dt_Rendelesek`
--

CREATE TABLE `dt_Rendelesek` (
  `_rendelesid` int(11) NOT NULL,
  `_menuid` int(11) NOT NULL,
  `_userid` int(11) NOT NULL,
  `_idopont` date NOT NULL,
  `_fizetesimod` int(1) NOT NULL,
  `_cim` varchar(100) DEFAULT NULL,
  `_telnumber` varchar(12) NOT NULL,
  `_kulonlegeskivansag` varchar(300) NOT NULL,
  `_mikorra` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dt_Rendelesek`
--

INSERT INTO `dt_Rendelesek` (`_rendelesid`, `_menuid`, `_userid`, `_idopont`, `_fizetesimod`, `_cim`, `_telnumber`, `_kulonlegeskivansag`, `_mikorra`) VALUES
(3, 6, 3, '2018-12-28', 1, 'Kisutca', '0759985211', '', '10:00:00'),
(4, 6, 3, '2018-12-28', 0, 'Simleu', '0759985215', '', '10:00:00'),
(8, 2, 3, '2018-12-28', 0, 'Kisutca 201b', '0759985211', '', '10:00:00'),
(11, 2, 3, '2018-12-28', 0, 'asd', '0759985215', '', '10:00:00'),
(15, 6, 3, '2018-12-28', 0, 'Nagyutca', '0759985211', '', '10:00:00'),
(32, 3, 3, '2019-01-03', 0, 'Kisutca 201b', '0759985215', '', '10:00:00'),
(33, 6, 3, '2019-01-03', 1, 'Kisutca 201b', '0759985215', '', '10:00:00'),
(34, 2, 3, '2019-01-03', 0, 'asdasd', '0765982563', '', '10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `dt_SzallitasiIdo`
--

CREATE TABLE `dt_SzallitasiIdo` (
  `_szallitasiidoid` int(11) NOT NULL,
  `_cim` varchar(30) DEFAULT NULL,
  `_szallitasiido` time DEFAULT NULL,
  `_szallitasiar` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dt_SzallitasiIdo`
--

INSERT INTO `dt_SzallitasiIdo` (`_szallitasiidoid`, `_cim`, `_szallitasiido`, `_szallitasiar`) VALUES
(1, 'Marasti', '00:30:00', '10'),
(2, 'Manastur', '00:28:00', '12'),
(3, 'Grigorescu', '01:00:00', '15');

-- --------------------------------------------------------

--
-- Table structure for table `dt_UserGroups`
--

CREATE TABLE `dt_UserGroups` (
  `_groupid` int(11) NOT NULL,
  `_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dt_UserGroups`
--

INSERT INTO `dt_UserGroups` (`_groupid`, `_name`) VALUES
(1, 'admin'),
(3, 'cook'),
(4, 'courier'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `dt_Users`
--

CREATE TABLE `dt_Users` (
  `_firstname` varchar(30) NOT NULL,
  `_lastname` varchar(30) NOT NULL,
  `_birthday` date NOT NULL,
  `_telnumber` varchar(12) NOT NULL,
  `_emailaddress` varchar(100) DEFAULT NULL,
  `_username` varchar(30) NOT NULL,
  `_password` varchar(64) NOT NULL,
  `_userid` int(11) NOT NULL,
  `_groupid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dt_Users`
--

INSERT INTO `dt_Users` (`_firstname`, `_lastname`, `_birthday`, `_telnumber`, `_emailaddress`, `_username`, `_password`, `_userid`, `_groupid`) VALUES
('Balog', 'Rainald', '2018-10-28', '0712345678', 'valami@ezaz.com', 'admin', '$2a$10$Dkv8XfHxqf7TPalevPTVU.pjiear7zR2TOKhVASHvVu5dIm4W5Yp6', 1, 1),
('Kiss', 'Adam', '2018-11-26', '0712345678', 'valami@ezaz.com', 'user', '$2a$10$X9YFie2EGPE94xvgPS/51eOQHeyp63gexQ6SO8m1tj0F2oBV6Wsci', 2, 2),
('Admin', 'Admina', '2018-11-25', '07827549274', 'valamivan@gmail.com', 'myuser', '$2a$10$NBoHfmgHwjJttqbeBlBXJ.Yq9WF1Dof3FQdpWSMT4HxxFJFGkFCMS', 3, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dt_Kategoriak`
--
ALTER TABLE `dt_Kategoriak`
  ADD PRIMARY KEY (`_kategoriaid`);

--
-- Indexes for table `dt_Kepek`
--
ALTER TABLE `dt_Kepek`
  ADD PRIMARY KEY (`_kepid`),
  ADD KEY `_menuid` (`_menuid`);

--
-- Indexes for table `dt_Megjegyzesek`
--
ALTER TABLE `dt_Megjegyzesek`
  ADD PRIMARY KEY (`_megjegyzesid`),
  ADD KEY `_menuid` (`_menuid`);

--
-- Indexes for table `dt_Menuk`
--
ALTER TABLE `dt_Menuk`
  ADD PRIMARY KEY (`_menuid`),
  ADD KEY `_kategoriaid` (`_kategoriaid`);

--
-- Indexes for table `dt_Rendelesek`
--
ALTER TABLE `dt_Rendelesek`
  ADD PRIMARY KEY (`_rendelesid`),
  ADD KEY `_menuid` (`_menuid`),
  ADD KEY `_userid` (`_userid`);

--
-- Indexes for table `dt_SzallitasiIdo`
--
ALTER TABLE `dt_SzallitasiIdo`
  ADD PRIMARY KEY (`_szallitasiidoid`),
  ADD UNIQUE KEY `_cim` (`_cim`);

--
-- Indexes for table `dt_UserGroups`
--
ALTER TABLE `dt_UserGroups`
  ADD PRIMARY KEY (`_groupid`),
  ADD UNIQUE KEY `_name` (`_name`);

--
-- Indexes for table `dt_Users`
--
ALTER TABLE `dt_Users`
  ADD PRIMARY KEY (`_userid`),
  ADD UNIQUE KEY `_username` (`_username`),
  ADD KEY `_groupid` (`_groupid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dt_Kategoriak`
--
ALTER TABLE `dt_Kategoriak`
  MODIFY `_kategoriaid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `dt_Kepek`
--
ALTER TABLE `dt_Kepek`
  MODIFY `_kepid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dt_Megjegyzesek`
--
ALTER TABLE `dt_Megjegyzesek`
  MODIFY `_megjegyzesid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `dt_Menuk`
--
ALTER TABLE `dt_Menuk`
  MODIFY `_menuid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `dt_Rendelesek`
--
ALTER TABLE `dt_Rendelesek`
  MODIFY `_rendelesid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `dt_SzallitasiIdo`
--
ALTER TABLE `dt_SzallitasiIdo`
  MODIFY `_szallitasiidoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dt_UserGroups`
--
ALTER TABLE `dt_UserGroups`
  MODIFY `_groupid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dt_Users`
--
ALTER TABLE `dt_Users`
  MODIFY `_userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dt_Kepek`
--
ALTER TABLE `dt_Kepek`
  ADD CONSTRAINT `dt_Kepek_ibfk_1` FOREIGN KEY (`_menuid`) REFERENCES `dt_Menuk` (`_menuid`);

--
-- Constraints for table `dt_Megjegyzesek`
--
ALTER TABLE `dt_Megjegyzesek`
  ADD CONSTRAINT `dt_Megjegyzesek_ibfk_1` FOREIGN KEY (`_menuid`) REFERENCES `dt_Menuk` (`_menuid`);

--
-- Constraints for table `dt_Menuk`
--
ALTER TABLE `dt_Menuk`
  ADD CONSTRAINT `dt_Menuk_ibfk_1` FOREIGN KEY (`_kategoriaid`) REFERENCES `dt_Kategoriak` (`_kategoriaid`);

--
-- Constraints for table `dt_Rendelesek`
--
ALTER TABLE `dt_Rendelesek`
  ADD CONSTRAINT `dt_Rendelesek_ibfk_1` FOREIGN KEY (`_menuid`) REFERENCES `dt_Menuk` (`_menuid`),
  ADD CONSTRAINT `dt_Rendelesek_ibfk_2` FOREIGN KEY (`_userid`) REFERENCES `dt_Users` (`_userid`);

--
-- Constraints for table `dt_Users`
--
ALTER TABLE `dt_Users`
  ADD CONSTRAINT `dt_Users_ibfk_1` FOREIGN KEY (`_groupid`) REFERENCES `dt_UserGroups` (`_groupid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
