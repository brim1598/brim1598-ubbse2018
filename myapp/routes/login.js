var express = require('express');
var app = express.Router();
var loginRender = require('../controllers/loginrender');
var userCheck = require('../controllers/usercheck');

//for Login

app.route('/')
    .post(function (request, response) {
        console.log('received a http request with POST method, parsing form data: /login');
        userCheck(request, response);
    })
    .get(function (request, response) {
        console.log('received a http request with GET method, parsing form data: /login');
        loginRender(request, response, '');
    });


module.exports = app;