module.exports = {
    insert: `INSERT INTO dt_Megjegyzesek(_menuid,_megjegyzes,_date) VALUES (?, ?, now())`,
    read: `SELECT * FROM dt_Megjegyzesek`,
    update: `UPDATE dt_Megjegyzesek SET ? WHERE _megjegyzesid = ?`,
    delete: `DELETE FROM dt_Megjegyzesek WHERE _megjegyzesid = ?`
}
