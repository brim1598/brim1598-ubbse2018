const mysql = require('promise-mysql');

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//MySQL kapcsolat letrehozasa

var configuration = {
  connectionLimit: 1000,
  connectTimeout: 60 * 60 * 1000,
  aquireTimeout: 60 * 60 * 1000,
  timeout: 60 * 60 * 1000,
  host: process.env.DATABASE_HOST || "localhost",
  port: process.env.MYSQL_PORT || "",
  user: process.env.MYSQL_USER || "root",
  password: process.env.MYSQL_PASSWORD || "root",
  database: process.env.MYSQL_DATABASE || "db_Catering"
};

module.exports = async () => {
  try {
      let pool;
      let con;
      if (pool) con = pool.getConnection();
      else {
          pool = await mysql.createPool(configuration);
          con = pool.getConnection();
      }
      return con;
  } catch (ex) {
      throw ex;
  }
};
