var MegjegyzesDAO = require('../dto/dao/megjegyzesdao');
var loginRender = require('../controllers/loginrender');

module.exports = async function (request, response) {
    if (request.session.logged == true) {
        if (request.body.comment.length != 0) {
            var megjegyzesDAO = new MegjegyzesDAO();
            await megjegyzesDAO.saveEntity([request.body.menuid, request.body.comment]);
            response.json('Comment inserted into database succesfully');
        }
        else {
            response.json('Comment textarea is empty!');
        }
    }
    else {
        loginRender(request, response, 'You are not logged in yet!');
    }
}

