const KategoriaDao = require("../dto/dao/kategoriadao");
const SzallitasiidoDao = require("../dto/dao/szallitasiidodao");
var loginRender = require('./loginrender');

function getCategories() {
    var kategoriaList = new KategoriaDao();
    var category = kategoriaList.readEntities();
    return category;
}

function getAddresses() {
    var adressList = new SzallitasiidoDao();
    var list = adressList.readEntities();
    return list;
}

module.exports = async function (request, response, text) {
    if (request.session.logged == true && request.session._groupid == 2) {
        dynamicdata = {};
        dynamicdata.category = await getCategories();
        dynamicdata.address = await getAddresses();

        response.render(text, dynamicdata, function (err, html) {
            if (err)
                console.log('Render error:' + err.toString());
            else
                response.send(html);
        });
    }
    else {
        console.log('There is no one signed in!');
        loginRender(request, response, 'You are not logged in yet!');
    }

}
