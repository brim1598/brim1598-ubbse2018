const dbConnection = require("../dbConnection");
const queries = require("../queries/kategoria");

module.exports = class KategoriaDao {
    async saveEntity(entity) {
        let con = await dbConnection();
        try {
            await con.query("START TRANSACTION");
            let savedTodo = await con.query(
                queries.insert, entity
            );
            await con.query("COMMIT");
            entity.id = savedTodo.insertId;
            return entity;
        } catch (ex) {
            await con.query("ROLLBACK");
            console.log(ex);
            throw ex;
        } finally {
            await con.release();
            await con.destroy();
        }
    }

    // what = obj
    async updateEntity(what, id) {
        let con = await dbConnection();
        try {
            await con.query("START TRANSACTION");
            await con.query(queries.update, [
                what,
                id
            ]);
            await con.query("COMMIT");
            return true;
        } catch (ex) {
            await con.query("ROLLBACK");
            console.log(ex);
            throw ex;
        } finally {
            await con.release();
            await con.destroy();
        }
    }

    async deleteEntity(id) {
        let con = await dbConnection();
        try {
            await con.query("START TRANSACTION");
            await con.query(queries.delete, [id]);
            await con.query("COMMIT");
            return true;
        } catch (ex) {
            await con.query("ROLLBACK");
            console.log(ex);
            throw ex;
        } finally {
            await con.release();
            await con.destroy();
        }
    }

    async readEntities() {
        let con = await dbConnection();
        try {
            await con.query("START TRANSACTION");
            let todo = await con.query(queries.read);
            await con.query("COMMIT");
            todo = JSON.parse(JSON.stringify(todo));
            return todo;
        } catch (ex) {
            console.log(ex);
            throw ex;
        } finally {
            await con.release();
            await con.destroy();
        }
    }
};
