module.exports = {
    insert: `INSERT INTO dt_Users SET ?`,
    read: `SELECT * FROM dt_Users`,
    update: `UPDATE dt_Users SET ? WHERE _userid = ?`,
    delete: `DELETE FROM dt_Users WHERE _userid = ?`
}
