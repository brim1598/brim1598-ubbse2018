var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var createError = require('http-errors');
var favicon = require('serve-favicon');

var homeRouter = require('./routes/home');
var guestRouter = require('./routes/guest')
var loginRouter = require('./routes/login');
var registrateRouter = require('./routes/registrate');
var logoutRouter = require('./routes/logout');
var addcommentRouter = require('./routes/addcomment');
var addorderRouter = require('./routes/addorder');
var selectedmenuRouter = require('./routes/selectedmenu')

var app = express();
// var server = http.createServer(app);
// console.log('Server started');

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//Session letrehozasa

app.use(session({
	secret: 'keyboard cat',
	resave: false,
	saveUninitialized: true
}))

//~~~~~~~~~~~~~~~~~~~

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));


app.use('/', loginRouter);
app.use('/registrate', registrateRouter);
app.use('/home', homeRouter);
app.use('/guest', guestRouter);
app.use('/logout', logoutRouter);
app.use('/addcomment',addcommentRouter);
app.use('/addorder',addorderRouter);
app.use('/home/selectedmenu',selectedmenuRouter);	// ???
app.use('/guest/selectedmenu',selectedmenuRouter);	//

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(function (req, res, next) {
	var logged = req.session.logged;
	if (!logged) {
		logged = req.session.logged = {};
	}
	next();
})

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;

app.listen(process.env.PORT || 3000);
