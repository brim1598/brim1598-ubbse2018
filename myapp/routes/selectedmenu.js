var express = require('express');
var app = express.Router();
var selected_menus = require('../controllers/selectmenus');


app.get('/', function (request, response, next) {
    console.log('received a http request with GET method, parsing form data: /selectedmenu');
});

app.post('/', function (request, response, next) {
    console.log('received a http request with POST method, parsing form data: /selectedmenu');
    selected_menus(request,response);
});

module.exports = app;