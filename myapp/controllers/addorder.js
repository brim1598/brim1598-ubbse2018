var OrderDAO = require('../dto/dao/rendelesdao');
var loginRender = require('./loginrender');
var homeRender = require('./homerender');

module.exports = async function (request, response) {

    if (request.session.logged == true) {
        if (request.body.menuid.length != 0 && request.body.address.length != 0 && request.body.phone.length != 0 && request.body.time.length != 0 && request.body.radio.length != 0) {

            var time = request.body.time;
            var hour = 0;
            var minute = 0;
            var plus = 0;
            if (time.slice(-2) == "PM") {
                plus = 12;
            }
            time = time.slice(0, -3);
            hour = time.split(':');
            minute = hour[1];
            hour = Number(hour[0]) + Number(plus);
            time = hour + ':' + minute;

            var rendelesDAO = new OrderDAO();
            await rendelesDAO.saveEntity([request.body.menuid, request.session._userid, request.body.radio, request.body.address, request.body.phone, request.body.specialreq, time]);
            console.log("Order inserted into database succesfully");
            homeRender(request, response, 'home');      // under repair...
        }
        else {
            response.json('Hiba az rendeles kitoltesenel!');
        }
    }
    else {
        console.log('received a http request with GET method, parsing form data: /addorder');
        loginRender(request, response, 'You are not logged in yet!');
    }
}

