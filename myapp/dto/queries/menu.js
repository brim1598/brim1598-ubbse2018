module.exports = {
    insert: `INSERT INTO dt_Menuk SET ?`,
    read: `SELECT * FROM dt_Menuk`,
    update: `UPDATE dt_Menuk SET ? WHERE _menuid = ?`,
    delete: `DELETE FROM dt_Menuk WHERE _menuid = ?`
}
