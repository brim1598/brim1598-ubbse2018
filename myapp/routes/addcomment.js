var express = require('express');
var app = express.Router();
var addcomment = require('../controllers/addcomment');
var loginRender = require('../controllers/loginrender');

app.route('/')
    .post(function (request, response) {
        console.log('received a http request with POST method, parsing form data: /addcomment');
        addcomment(request, response);
    })
    .get(function (request, response) {
        console.log('received a http request with GET method, parsing form data: /addcomment');
        loginRender(request, response, 'You are not logged in yet!');
    });


module.exports = app;
