module.exports = {
    insert: `INSERT INTO dt_UserGroups SET ?`,
    read: `SELECT * FROM dt_UserGroups`,
    update: `UPDATE dt_UserGroups SET ? WHERE _groupid = ?`,
    delete: `DELETE FROM dt_UserGroups WHERE _groupid = ?`
}
