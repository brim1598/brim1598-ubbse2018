const MenuDao = require("../dto/dao/menudao");
const FeedbacksDao = require("../dto/dao/megjegyzesdao");
const ImagesDao = require("../dto/dao/kepekdao");

function getImages() {
    var adressList = new ImagesDao();
    var list = adressList.readEntities();
    return list;
}

function getFeedbacks() {
    var feedbacksList = new FeedbacksDao();
    var list = feedbacksList.readEntities();
    return list;
}

function getMenus() {
    var mList = new MenuDao();
    var list = mList.readEntities();
    return list;
}


module.exports = async function (request, response) {
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

    dynamicdata = {};
    var menus;
    var feedbacks;
    var photos;

    var tempmenu;

    if (request.body.selected_category != '0') {
        tempmenu = await getMenus();
        menus = tempmenu.filter((value, index, array) => {
            return value._kategoriaid == request.body.selected_category;
        });
    }
    else {
        menus = await getMenus();
    }

    photos = await getImages();
    feedbacks = await getFeedbacks();

    dynamicdata.menus = menus;
    for (var i = 0; i < menus.length; i++) {
        dynamicdata.menus[i].feedback = [];
        dynamicdata.menus[i].photos = [];
        var index = 0;
        for (var ii = 0; ii < feedbacks.length; ii++) {
            if (menus[i]._menuid == feedbacks[ii]._menuid) {
                dynamicdata.menus[i].feedback[index] = {};
                dynamicdata.menus[i].feedback[index]._megjegyzes = feedbacks[ii]._megjegyzes;
                dynamicdata.menus[i].feedback[index]._date = new Date(feedbacks[ii]._date).toLocaleDateString("hu-HU", options);
                index++;
            }
        }
        index = 0;
        for (var ii = 0; ii < photos.length; ii++) {
            if (menus[i]._menuid == photos[ii]._menuid) {
                dynamicdata.menus[i].photos[index] = {};
                dynamicdata.menus[i].photos[index]._path = photos[ii]._path;
                index++;
            }
        }
    }
    response.json(dynamicdata);
};

