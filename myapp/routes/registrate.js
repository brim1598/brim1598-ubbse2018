var express = require('express');
var app = express.Router();
var registrationCheck = require('../controllers/registrationcheck');
var registrateRender = require('../controllers/registraterender');


//for Registration

app.get('/', function (request, response, next) {
	console.log('received a http request with GET method, parsing form data: /registrate');
	registrateRender(request, response, '');
});

app.post('/', function (request, response, next) {
	console.log('received a http request with POST method, parsing form data: /registrate');
	registrationCheck(request, response);
});

module.exports = app;