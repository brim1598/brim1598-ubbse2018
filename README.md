# Catering company

This is a website where you can order menus at any time.

## Built With

* [Nodejs](https://nodejs.org/en/about/) - The web framework used
* [Mysql](https://www.mysql.com/) - Database Management

## Authors

* **Balog Rainald** - *Initial work* - [PurpleBooth](https://gitlab.com/brim1598)

## License

There is no need for this.
