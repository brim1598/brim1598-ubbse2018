module.exports = {
    insert: `INSERT INTO dt_Rendelesek(_idopont, _menuid, _userid, _fizetesimod, _cim, _telnumber, _kulonlegeskivansag, _mikorra) VALUES (now(), ?, ?, ?, ?, ?, ?, ?)`,
    read: `SELECT * FROM dt_Rendelesek`,
    update: `UPDATE dt_Rendelesek SET ? WHERE _rendelesid = ?`,
    delete: `DELETE FROM dt_Rendelesek WHERE _rendelesid = ?`
}
