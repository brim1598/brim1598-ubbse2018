var express = require('express');
var app = express.Router();
var guestRender = require('../controllers/guestrender');

app.route('/')
    .post(function (request, response) {
        console.log('received a http request with POST method, parsing form data: /guest');
    })
    .get(function (request, response) {
        console.log('received a http request with GET method, parsing form data: /guest');
        guestRender(request, response, 'guest');
    });


module.exports = app;