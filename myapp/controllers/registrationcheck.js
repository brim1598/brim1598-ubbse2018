var bcrypt = require('bcryptjs');
var saltRounds = 10;
var UsersDAO = require('../dto/dao/userdao');
var registrateRender = require('./registraterender');
var loginRender = require('./loginrender');

module.exports = async function (request, response) {

    var registrateList = {
        _firstname: request.body._firstname,
        _lastname: request.body._lastname,
        _birthday: request.body._bday,
        _telnumber: request.body._telnumber,
        _emailaddress: request.body._email,
        _username: request.body._username,
        _password: request.body._password,
        _groupid: 2 	// user
    };



    var usersDAO = new UsersDAO();
    var usersList = await usersDAO.readEntities();
    var result = usersList.filter((value, index, array) => {
        return value._username == registrateList['username'];
    });

    if (!result.length) {
        var salt = bcrypt.genSaltSync(saltRounds);
        var hash = bcrypt.hashSync(registrateList["_password"], salt);
        registrateList["_password"] = hash;
        await usersDAO.saveEntity(registrateList);
        console.log('Successful registration!');
        loginRender(request, response, 'Registration succeded! Now login!');
    }
    else {
        console.log('User already exists!\n');
        registrateRender(request, response, 'Username already exists!');
    }
}