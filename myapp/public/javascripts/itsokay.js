//registration

class Inputs {
    constructor() {
        this.inputs = {
            firstname: false,
            lastname: false,
            telnumber: false,
            email: false,
            username: false,
            password: false
        }
    }

    isEmpty(str) {
        return str === '';
    }

    firstname(str) {
        this.inputs.firstname = false;
        if (this.isEmpty(str.value)) {
            document.getElementById("_error").innerHTML = "Kerlek toltsd ki!";
        }
        else {
            document.getElementById("_error").innerHTML = '';
            this.inputs.firstname = true;
        }
    }

    lastname(str) {
        this.inputs.lastname = false;
        if (this.isEmpty(str.value)) {
            document.getElementById("_error").innerHTML = "Kerlek toltsd ki!";
        }
        else {
            document.getElementById("_error").innerHTML = '';
            this.inputs.lastname = true;
        }
    }

    bday(str) {
        this.inputs.bday = false;
        if (this.isEmpty(str.value)) {
            document.getElementById("_bday").style.webkitTextFillColor = "red";
            document.getElementById("_error").innerHTML = "Kerlek toltsd ki!";
        }
        else {
            if (new Date(str.value).getTime() > new Date().getTime()) {
                document.getElementById("_bday").style.webkitTextFillColor = "red";
            }
            else {
                document.getElementById("_bday").style.webkitTextFillColor = "green";
                document.getElementById("_error").innerHTML = '';
                this.inputs.bday = true;
            }
        }
    }

    telnumber(str) {
        this.inputs.telnumber = false;
        if (this.isEmpty(str.value)) {
            document.getElementById("_error").innerHTML = "Kerlek toltsd ki! (min10 - max12 szamjegy)";
        }
        else {
            var regex = /^([0-9])+$/;
            if (str.value.match(regex)) {
                if (str.value.length > 9 && str.value.length < 13) {
                    document.getElementById("_error").innerHTML = '';
                    this.inputs.telnumber = true;
                }
            }
            else {
                document.getElementById("_error").innerHTML = "Csak szamjegyet tartalmazzon! (min10 - max12 szamjegy)";
            }
        }
    }

    email(str) {
        this.inputs.email = false;
        if (this.isEmpty(str.value)) {
            document.getElementById("_error").innerHTML = "Kerlek toltsd ki!";
        }
        else {
            var regex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
            if (str.value.match(regex)) {
                document.getElementById("_error").innerHTML = '';
                this.inputs.email = true;
            }
            else {
                document.getElementById("_error").innerHTML = "Nem helyes email cim!";
            }
        }
    }

    username(str) {
        this.inputs.username = false;
        if (this.isEmpty(str.value)) {
            document.getElementById("_error").innerHTML = "Kerlek toltsd ki!\nCsak kisbetukbol(max. 10) allhat!";
        }
        else {
            var regex = /^([a-z])+$/;
            if (str.value.match(regex)) {
                if (str.value.length >= 11) {
                    document.getElementById("_error").innerHTML = "Tul hosszu felhasznalonevnek!";
                }
                else {
                    document.getElementById("_error").innerHTML = '';
                    this.inputs.username = true;
                }
            }
            else {
                document.getElementById("_error").innerHTML = "Csak kisbetukbol(max. 10) allhat!";
            }
        }
    }

    password(str) {
        this.inputs.password = false;
        if (this.isEmpty(str.value)) {
            document.getElementById("_error").innerHTML = "Kerlek toltsd ki!\nCsak kisbetukbol(max. 10) allhat!";
        }
        else {
            var regex1 = /(([a-zA-Z])+([1-9])+([a-zA-Z1-9])*)+$/;
            var regex2 = /(([1-9])+([a-zA-Z])+([a-zA-Z1-9])*)+$/;
            if (str.value.match(regex1) || str.value.match(regex2)) {
                if (str.value.length >= 11) {
                    document.getElementById("_error").innerHTML = "Tul hosszu jelszonak!";
                }
                else {
                    document.getElementById("_error").innerHTML = '';
                    this.inputs.telnumber = true;
                }
            } else {
                document.getElementById("_error").innerHTML = "Betut es szamot is kell tartalmaznia, hossza max. 10 karakter!";
            }
        }
    }

    password_again(str) {
        this.inputs.password = false;
        if (this.isEmpty(str.value)) {
            document.getElementById("_error").innerHTML = "Kerlek toltsd ki!\nCsak kisbetukbol(max. 10) allhat!";
        }
        else {
            if (document.getElementById("_password").value == document.getElementById("_password_again").value) {
                document.getElementById("_error").innerHTML = '';
                this.inputs.password = true;
            }
            else {
                document.getElementById("_error").innerHTML = "Nem egyezik a jelszo!";
            }
        }
    }

    isValid() {
        var x = true;
        document.getElementById("_error").innerHTML = '';
        for (var value in this.inputs) {
            if (this.inputs[value] == false) {
                x = false;
                break;
            }
        }
        if (x) {
            document.getElementById("_submitButton").disabled = false;
        }
        else {
            console.log(this.inputs);
            document.getElementById("_error").innerHTML = "Kerlek Nezd at megegyszer!";
        }
    }

    logusername(str) {
        if (this.isEmpty(str.value)) {
            document.getElementById("_loginerror").innerHTML = "Kerlek toltsd ki!";
        }
    }

    logpassword(str) {
        if (this.isEmpty(str.value)) {
            document.getElementById("_loginerror").innerHTML = "Kerlek toltsd ki!";
        }
    }
}

var val = new Inputs();
