module.exports = {
    insert: `INSERT INTO dt_SzallitasiIdo SET ?`,
    read: `SELECT * FROM dt_SzallitasiIdo`,
    update: `UPDATE dt_SzallitasiIdo SET ? WHERE _szallitasiidoid = ?`,
    delete: `DELETE FROM dt_SzallitasiIdo WHERE _szallitasiidoid = ?`
}
