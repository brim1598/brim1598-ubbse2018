function getcateg() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/home/selectedmenu',
        data: {
            selected_category: $('#select_category').val(),
        },
        success: function (data) {
            $('#menu-content').html('');
            $.each(data, function () {
                $.each(this, function (key, value) {
                    var divclass = '  <div class="column">';
                    var class_content = '<div class="content">';
                    var messages = '<div class="messages">';
                    var messfeedback = '<div class="messfeedback">'
                    var container = '<div class="container">';
                    var dialbox = '<div class="dialogbox">';
                    var bodyclass = '<div class="body">';
                    var spanclass = '<span class="tip tip-left"></span>';
                    var commentclass = '<div class="comment">';
                    var titleid = '<div class="titleandprice">';
                    var classtitle = '<div class="menutitle">';
                    var classprice = '<div class="price">';
                    var menuid = '';
                    var title = '';
                    var feedbacks = '';
                    var images = '';
                    var description = '';
                    var price = '';
                    var time = '';
                    var category = '';
                    var toappend = '';

                    $.each(value, function (key, value) {
                        if (key == '_menuid') {
                            menuid += value;
                        }
                        if (key == '_kategorianev') {
                            category += value;
                        }
                        if (key == '_menunev') {
                            title += value;
                        }
                        if (key == '_leiras') {
                            description += '<p>' + value + '</p>';
                        }
                        if (key == '_ar') {
                            price += value;
                        }
                        if (key == '_elkeszitesiido') {
                            time += '<label>Elkeszitesi ido:&nbsp;&nbsp;&nbsp;' + value + '</label>';
                        }
                        if (key == 'feedback') {
                            $.each(value, function (key, value) {
                                $.each(value, function (key, value) {
                                    if (key == '_megjegyzes') {
                                        feedbacks += dialbox + bodyclass + spanclass + commentclass
                                            + '<span>' + value + '</span>';
                                    }
                                    if (key == "_date") {
                                        feedbacks += '<br><sub>' + value + '</sub>' + '</div></div></div>';
                                    }
                                });
                            });
                        }
                        if (key == 'photos') {
                            $.each(value, function (key, value) {
                                $.each(value, function (key, value) {
                                    if (key == '_path') {
                                        images += value;
                                    }
                                });
                            });
                        }
                    });
                    if (feedbacks != '') {
                        feedbacks = '<h5>Visszajelzesek:</h5>'
                            + messfeedback + container + feedbacks + '</div></div>';
                    }

                    var form = '<div><div class="formdiv"><textarea class="comment" ng-model="loremIpsum" ng-keyup="autoExpand($event)"'
                        + 'placeholder="Mi a velemenyed az etelrol?"></textarea>'
                        + '<input type="hidden" name="menuid" value="' + menuid + '">'
                        + '<button class="textareabutton" value="Submit">Submit</button></div></div>';

                    toappend += divclass + class_content + '<img class = "imageclass" src="' + images + '" alt="' + title
                        + '">' + titleid + classprice + '<h4>Ar: ' + price + ' Eur</h4>' + '</div>'
                        + classtitle + '<h4>' + title + '</h4>' + '</div></div>'
                        + messages + description + time
                        + '</div>' + feedbacks + '<br>' + form
                        + '<a href="#" class="buybutton">Rendelj Most</a>'
                        + '<input type="hidden" name="menuid" value="' + menuid + '">' + '</div></div>';
                    $('#menu-content').append(toappend);
                });
            });
        }
    });
}


$(function () {
    $('#select_category').change(function () {
        getcateg();
    });
});

function addcomment(that, args) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/addcomment',
        data: {
            menuid: args[0],
            comment: args[1],
        },
        success: function (params) {
            // console.log(params);
            alert('Koszonjuk a visszajelzeset!');
            $(that).prevAll('.comment').val('');
        }
    });
}

$(document).on('click', '.textareabutton', function () {
    addcomment(this, [$(this).prevAll('[name="menuid"]').val(), $(this).prevAll('.comment').val()]);
});



// Modal form 
$(document).on('click', '.buybutton', function () {
    $('.modalcontactForm').fadeToggle();
    $(document).mouseup(function (e) {
        var container = $(".modalcontactForm");
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.fadeOut();
        }
    });
});

$("form").submit(function (e) {
    var form = $(this);
    $.ajax({
        url: form.attr('action'),
        type: form.attr('method'),
        dataType: 'json',
        data: form.serialize(), // data to be submitted
        success: function (response) {
            alert('Koszonjuk hogy nalunk rendelt!'); 
            $('input:hidden[name="menuid"]').remove();
        }
        // Help me !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    });
    return false;
});

$(document).on('click', '.buybutton', function () {
    $('form').append('<input type="hidden" name="menuid" value="' + $(this).next('[name="menuid"]').val() + '">');
});

$(document).ready(function () {
    $('#timepicker').mdtimepicker({
        // format of the time value (data-time attribute)
        timeFormat: 'hh:mm:ss.000',
        // format of the input value
        format: 'h:mm tt',
        // theme of the timepicker
        // 'red', 'purple', 'indigo', 'teal', 'green'
        theme: 'blue',
        // determines if input is readonly
        readOnly: true,
        // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
        hourPadding: false
    });

    $('#timepicker').mdtimepicker().on('timechanged', function (e) {
        // console.log(e.value);
        // console.log(e.time);
    });
});



