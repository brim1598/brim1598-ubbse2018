var bcrypt = require('bcryptjs');
const UsersDao = require("../dto/dao/userdao");
var loginRender = require('./loginrender');
var homeRender = require('./homerender');


module.exports = async function (request, response) {
    var username = request.body._loginusername;
    var password = request.body._loginpassword;

    console.log(username);
    console.log(password);

    var userDAO = new UsersDao();
    var userslist = await userDAO.readEntities();
    var result = userslist.filter((value, index, array) => {
        var passwordColumn = value._password;
        return value._username == username && bcrypt.compareSync(password, passwordColumn);
    });
    if (result.length) {
        request.session.logged = true;
        request.session._groupid = result[0]._groupid;
        request.session._userid = result[0]._userid;

        var str = '';
        switch (result[0]._groupid) {
            case 1:
                str = '(admin)';
                var text = 'Welcome, ' + username + ' ' + str + ' ! ';
                console.log(text);
                adminRender(request, response, 'admin', '', '', '', '');
                break;
            case 2:
                str = '(user)';
                var text = 'Welcome, ' + username + ' ' + str + ' ! ';
                console.log(text);
                homeRender(request, response, 'home');
                break;
            case 3:
                str = '(cook)';
                var text = 'Welcome, ' + username + ' ' + str + ' ! ';
                console.log(text);
                cookRender(request, response, 'cook', '', '', '', '');
                break;
            case 4:
                str = '(courier)';
                var text = 'Welcome, ' + username + ' ' + str + ' ! ';
                console.log(text);
                courierRender(request, response, 'courier', '', '', '', '');
                break;
            default: break;
        }
    }
    else {
        console.log('User do not exist or incorrect password');
        loginRender(request, response, 'Incorrect username or password!');
    }
}
