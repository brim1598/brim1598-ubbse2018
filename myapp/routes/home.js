var express = require('express');
var app = express.Router();
var homeRender = require('../controllers/homerender');

app.get('/', function (request, response, next) {
    console.log('received a http request with GET method, parsing form data: /home');
    homeRender(request, response, 'home');
});

app.post('/', function (request, response, next) {
    console.log('received a http request with POST method, parsing form data: /home');
});

module.exports = app;
