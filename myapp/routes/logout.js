var express = require('express');
var app = express.Router();
var loginRender = require('../controllers/loginrender');

//Logout

app.get('/', function (request, response, next) {
	if (request.session.logged == true) {
		request.session.destroy();
		console.log('User logged out!');
		loginRender(request, response, 'You logged out!');
	}
	else {
		console.log('Can not logout! There is no one signed in!');
		loginRender(request, response, 'Can not log out! You are not logged in yet!');
	}
});

module.exports = app;