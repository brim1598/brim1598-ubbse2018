module.exports = {
    insert: `INSERT INTO dt_Kategoriak SET ?`,
    read: `SELECT * FROM dt_Kategoriak`,
    update: `UPDATE dt_Kategoriak SET ? WHERE _kategoriaid = ?`,
    delete: `DELETE FROM dt_Kategoriak WHERE _kategoriaid = ?`
}
