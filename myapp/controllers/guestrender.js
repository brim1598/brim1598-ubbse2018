const KategoriaDao = require("../dto/dao/kategoriadao");

function getCategories() {
    var kategoriaList = new KategoriaDao();
    var category = kategoriaList.readEntities();
    return category;
}

module.exports = async function (request, response, text) {
    dynamicdata = {};
    dynamicdata.category = await getCategories();

    response.render(text, dynamicdata, function (err, html) {
        if (err)
            console.log('Render error:' + err.toString());
        else
            response.send(html);
    });
}
