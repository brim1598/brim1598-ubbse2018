module.exports = {
    insert: `INSERT INTO dt_Kepek SET ?`,
    read: `SELECT * FROM dt_Kepek`,
    update: `UPDATE dt_Kepek SET ? WHERE _kepid = ?`,
    delete: `DELETE FROM dt_Kepek WHERE _kepid = ?`
}
