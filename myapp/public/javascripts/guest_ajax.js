function get() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/guest/selectedmenu',
        data: {
            selected_category: $('#select_category').val(),
        },
        success: function (data) {
            $('#menu-content').html('');
            $.each(data, function () {
                $.each(this, function (key, value) {
                    var divclass = '  <div class="column">';
                    var class_content = '<div class="content">';
                    var messages = '<div class="messages">';
                    var messfeedback = '<div class="messfeedback">'
                    var container = '<div class="container">';
                    var dialbox = '<div class="dialogbox">';
                    var bodyclass = '<div class="body">';
                    var spanclass = '<span class="tip tip-left"></span>';
                    var commentclass = '<div class="comment">';
                    var titleid = '<div id = "titleandprice">';
                    var classtitle = '<div class="menutitle">';
                    var classprice = '<div class="price">';
                    var title = '';
                    var feedbacks = '';
                    var images = '';
                    var description = '';
                    var category = '';
                    var toappend = '';

                    $.each(value, function (key, value) {
                        if (key == '_kategorianev') {
                            category += value;
                        }
                        if (key == '_menunev') {
                            title += value;
                        }
                        if (key == '_leiras') {
                            description += '<p>' + value + '</p>';
                        }
                        if (key == 'feedback') {
                            $.each(value, function (key, value) {
                                $.each(value, function (key, value) {
                                    if (key == '_megjegyzes') {
                                        feedbacks += dialbox + bodyclass + spanclass + commentclass
                                            + '<span>' + value + '</span>';
                                    }
                                    if (key == "_date") {
                                        feedbacks += '<br><sub>' + value + '</sub>' + '</div></div></div>';
                                    }
                                });
                            });
                        }
                        if (key == 'photos') {
                            $.each(value, function (key, value) {
                                $.each(value, function (key, value) {
                                    if (key == '_path') {
                                        images += value;
                                    }
                                });
                            });
                        }
                    });
                    if (feedbacks != '') {
                        feedbacks = '<h5>Visszajelzesek:</h5>'
                            + messfeedback + container + feedbacks + '</div></div>';
                    }
                    toappend += divclass + class_content + '<img class = "imageclass" src="' + images + '" alt="' + title
                        + '">' + titleid
                        + classtitle + '<h4>' + title + '</h4>' + '</div></div>'
                        + messages + description
                        + '</div>' + feedbacks + '</div></div>';
                    $('#menu-content').append(toappend);
                });
            });
        }
    });
}


$(function () {
    $('#select_category').change(function () {
        get();
    });
});
