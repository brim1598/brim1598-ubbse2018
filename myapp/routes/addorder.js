var express = require('express');
var app = express.Router();
var addorder = require('../controllers/addorder');
var loginRender = require('../controllers/loginrender');

app.route('/')
    .post(function (request, response) {
        console.log('received a http request with POST method, parsing form data: /addorder');
        addorder(request,response);
    })
    .get(function (request, response) {
        console.log('received a http request with GET method, parsing form data: /addorder');
        loginRender(request, response, 'You are not logged in yet!');
    });


module.exports = app;